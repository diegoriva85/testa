
require('./bootstrap');

window.Vue = require('vue');

Vue.component('content-component', require('./components/contentComponent.vue').default);
Vue.component('form-component', require('./components/formComponent.vue').default);
Vue.component('row-component', require('./components/rowComponent.vue').default);


const app = new Vue({
    el: '#app',
});
