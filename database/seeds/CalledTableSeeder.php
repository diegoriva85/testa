<?php

use Illuminate\Database\Seeder;
use App\Called;
class CalledTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          $data = array(
               ['elevator_number' => 1,
                'flors'=> 2,
                'start' => 540,
                'end'=> 660,
                'frequency'=> 5],

                ['elevator_number' => 2,
                'flors'=> 1,
                'start' => 540,
                'end'=> 600,
                'frequency'=> 10],

                ['elevator_number' => 3,
                'flors'=> 123,
                'start' => 660,
                'end'=> 1100,
                'frequency'=> 20],

                ['elevator_number' => 1,
                'flors'=> 123,
                'start' => 840,
                'end'=> 900,
                'frequency'=> 5]
                    );

        Called::insert($data);
    }
}
